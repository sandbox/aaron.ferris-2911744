Description
===============================================================================
This module adds the Thunderhead script to pages unless configured to be excluded.

Requirements
===============================================================================
None

Configuration
===============================================================================
1. Enable the module
2. Configure permissions for Thunderhead
3. Enable the Thunderhead option within the admin configuration
4. Enter the Thunderhead script URL
5. Enter a Thunderhead site key
6. Enter any excluded URLs
6. Save the settings
